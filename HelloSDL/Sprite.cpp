//
//  Sprite.cpp
//  SDL Skeleton
//
//  Created by Evan Chapman on 8/24/13.
//  Copyright (c) 2013 Student. All rights reserved.
//

#include <vector>
#include "Sprite.h"


Sprite::Sprite(){
    image = NULL;
    isClipped = false;
    isBoundary = false;

    isSafe = true;
    isBad = false;
    isCollectable = false;
    
    isSeeking = false;
    
    doesCollide = false;
    doesScroll = false;
    
    collisionMap = NULL;
    
    cameraOffset.x = 0;
    cameraOffset.y = 0;
    
    velX = 0;
    velY = 0;
    
    accelX = 0;
    accelY = 0;
    
    maxVelX = 1;
    maxVelY = 1;
    
    dragX = 0;
    dragY = 0;
    
    position.x = 10;
    position.y = 10;
    
    isDrawn = true;
    
}

bool Sprite::loadImage(std::string filename){
    SDL_Surface *loadedImage = NULL;

    
    loadedImage = IMG_Load(filename.c_str());
    
    if (loadedImage != NULL) {
        image = SDL_DisplayFormatAlpha(loadedImage);
        position.w = loadedImage->w;
        position.h = loadedImage->h;
        
        SDL_FreeSurface(loadedImage);
        
    } else {
        return false;
        printf("Failed to load image... \n");
    }
    
    return true;
}

void Sprite::draw(SDL_Surface* screen){
    

    if (isDrawn){
        if (!isClipped){
            SDL_BlitSurface(image,NULL, screen , &position);
        } else {
            SDL_BlitSurface(image,&clip, screen , &position);
        }
    }
}

void Sprite::Update(float deltaTime){
    
    if (velX > maxVelX){
        velX = maxVelX;
    }
    
    if (velX < -maxVelX){
        velX = -maxVelX;
    }
    
    
    if (velY > maxVelY){
        velY = maxVelY;
    }
    
    
    if (velY < -maxVelX){
        velY = -maxVelY;
    }
    
    
    if (velX > 0 ){
        velX -= dragX;
        
        if (velX < dragX){
            velX = 0;
        }
    }
    
    if (velX < 0 ){
        velX += dragX;
        
        if (velX > dragX){
            velX = 0;
        }
        
    }
    
    if (velY > 0 ){
        velY -= dragY;
        
        if (velY < dragY){
            velY = 0;
        }
    }
    
    if (velY < 0 ){
        velY += dragY;
        
        if (velY > dragY){
            velY = 0;
        }
    }

    if (velX == 0 && velY == 0){
        hasStopped = false;
        if (isMoving){
            hasStopped = true;
        }
        isMoving = false;
    }
    
    
    finalX = velX *deltaTime;
    
    //Update position
    position.x +=finalX;
    
    //adjust based on collisions
    
    if (doesCollide) {
        if(checkCollisions()){
            position.x -=finalX;
           // printf("Moved back X: %i " , finalX);
        }
    }
    
    finalY = velY *deltaTime;
    
    position.y +=finalY;
    
    //adjust based on collisions
    if (doesCollide){
        if(checkCollisions()){
            position.y -=finalY;
           // printf("Moved back Y: %i " , finalY);
        }
    }
    
}



bool Sprite::checkCollisions(){
    if (collisionMap!= NULL) {
        vector<Sprite*>::iterator it;
        for (it = collisionMap->begin(); it != collisionMap->end() ; it++ ){
            if (detectCollision((*it)->position, position)){
                return true;
            }
            
        }
    }
    return false;
}


bool Sprite::detectCollision(SDL_Rect a , SDL_Rect b){
    //Compute left right top bottom of A and
    int leftA , rightA , topA, bottomA ,leftB , rightB , topB, bottomB;
    
    leftA = a.x;
    rightA = a.x + a.w;
    topA = a.y;
    bottomA = a.y + a.h;
    
    leftB = b.x;
    rightB = b.x + b.w;
    topB = b.y;
    bottomB = b.y + b.h;
    
    //If any of the sides from A are outside of B
    if( bottomA <= topB )
    {
        return false;
    }
    
    if( topA >= bottomB )
    {
        return false;
    }
    
    if( rightA <= leftB )
    {
        return false;
    }
    
    if( leftA >= rightB )
    {
        return false;
    }
    
    //If none of the sides from A are outside B
    //printf("Did collide! \n");
    return true;
}


