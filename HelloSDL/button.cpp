//
//  button.cpp
//  SDL Skeleton
//
//  Created by Evan Chapman on 8/24/13.
//  Copyright (c) 2013 Student. All rights reserved.
//

#include "button.h"


void Button::press(){
    if (isPressed){
        isHeld = true;
    }
    isPressed = true;
}

void Button::release(){
    if (isHeld){
        isReleased = true;
    } else {
        isReleased = false;
    }
    isPressed = false;
    isHeld = false;
}