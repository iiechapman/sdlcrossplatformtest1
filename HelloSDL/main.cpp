//
// SDL Skeleton By Evan Chapman
// Simple Test Loads Image to Screen
// New Version Updated for VS2013
// Now moved headers to proper OS pre-processor
//

#include "CrossDependencies.h"

#include <vector>

#include "main.h"
#include "button.h"
#include "Sprite.h"

//Main Application Definitions
#define WIDTH 1024
#define HEIGHT 768
#define BPP     32
#define DEBUGMODE 0


using namespace std;

string WINDOW_TITLE = "LD27 - FSCK";

bool introPlayed = false;

//Create surfaces for drawable objects
SDL_Surface *screen = NULL;
SDL_Surface *sprite = NULL;
SDL_Surface *robotUp = NULL;
SDL_Surface *robotDown = NULL;
SDL_Surface *robotLeft = NULL;
SDL_Surface *robotRight = NULL;

SDL_Surface *errorRight = NULL;
SDL_Surface *errorLeft = NULL;
SDL_Surface *errorUp = NULL;
SDL_Surface *errorDown = NULL;

SDL_Surface *shiftRight = NULL;
SDL_Surface *shiftLeft = NULL;
SDL_Surface *shiftUp = NULL;
SDL_Surface *shiftDown = NULL;

SDL_Surface *scoutRight = NULL;
SDL_Surface *scoutLeft = NULL;
SDL_Surface *scoutUp = NULL;
SDL_Surface *scoutDown = NULL;

SDL_Surface *trailImage = NULL;

SDL_Surface *robotScout = NULL;
SDL_Surface *robotShift = NULL;
SDL_Surface *robotShiftError = NULL;
SDL_Surface *orbImageActive = NULL;
SDL_Surface *orbImageInactive = NULL;
SDL_Surface *charge = NULL;
SDL_Surface *charge2 = NULL;
SDL_Surface *charge3 = NULL;
SDL_Surface *charge4 = NULL;
SDL_Surface *emptyCharge = NULL;

Sprite *c[10];
Sprite *cEmpty[10];

SDL_Surface *divider = NULL;
SDL_Surface *message = NULL;
SDL_Surface *tile0 = NULL;
SDL_Surface *tile1 = NULL;
SDL_Surface *trap = NULL;
SDL_Surface *dataImage = NULL;
SDL_Surface *finalDataImage = NULL;

SDL_Surface *headImage = NULL;

SDL_Surface *vinScreen = NULL;
SDL_Surface *detScreen = NULL;
SDL_Surface *ldScreen = NULL;
SDL_Surface *introScreen = NULL;
SDL_Surface *titleScreen = NULL;
SDL_Surface *levelSelectScreen = NULL;
SDL_Surface *controlsScreen = NULL;
SDL_Surface *quitScreen = NULL;
SDL_Surface *deathBubble = NULL;
SDL_Surface *tearImage = NULL;


SDL_Surface *bitLarge = NULL;
SDL_Surface *xboxActive = NULL;
SDL_Surface *xboxInactive = NULL;

Sprite* controllerIcon = NULL;
Sprite* spriteBubble = NULL;
Sprite* head = NULL;

Sprite* tear = NULL;
int tearTimer = 0;
int tearTime = 200;

int coolDownTimer = 0;
int coolDownTime = 1000;


//Create soundbuffers
Mix_Music *levelIntroMusic = NULL;
Mix_Music *levelMusic = NULL;
Mix_Music *titleMusic = NULL;


Mix_Chunk *sound1 = NULL;
Mix_Chunk *startupSound = NULL;
Mix_Chunk *resetSound = NULL;
Mix_Chunk *startLevel = NULL;
Mix_Chunk *heartbeat = NULL;
Mix_Chunk *movingSound = NULL;
Mix_Chunk *stoppingSound = NULL;
Mix_Chunk *collectableSound = NULL;
Mix_Chunk *resetScoutSound = NULL;
Mix_Chunk *resetShiftSound = NULL;
Mix_Chunk *enterScoutSound = NULL;
Mix_Chunk *enterShiftSound = NULL;
Mix_Chunk *shiftErrorSound = NULL;
Mix_Chunk *countDownSound = NULL;
Mix_Chunk *activateSound = NULL;
Mix_Chunk *endTimeSound = NULL;
Mix_Chunk *fragFound = NULL;
Mix_Chunk *highScoreSound = NULL;
Mix_Chunk *quitSound = NULL;

//Font setup
TTF_Font *font = NULL;
SDL_Color   textColor = {57,214,0};
SDL_Color   whiteColor = {255,255,255};

//Sections of sprite sheet
SDL_Rect    rcSprite;
SDL_Rect    rcRobot;
SDL_Rect    rcRobotScout;

//Camera Code
SDL_Rect cameraPosition;

SDL_Rect startingPosition;

int fullClip = 10;
SDL_Rect    clip[100];

//Animation timer section
int frame = 0;
int fullTimer = 3;
int timer = fullTimer;

int shiftDuration = 2;
int scoutDuration = 5;

int trailDistanceMax = 0;
int trailDistance = .01;

//Global Timing Section
float startTime = 0;
float deltaTime = 0;
float animDelta = 0 ;

bool death = false;

bool joystickActive = false;

//Buttons
Button button1;
Button button2;

Button joyButton1;
Button joyButton2;

Button mouseButton1;
Button mouseButton2;

Button upButton;
Button downButton;

Button escapeButton;

Button enterButton;

Button fullScreenButton;


//Globals
char timeBuffer[1000];
//bool done = false;
bool running = true;
int seconds;
int totalSeconds = 11;
int currentSeconds = 11;
int totalFrames;
int FPS;
int capFPS = 20;

bool FULLSCREEN = false;

//difficulty variables
int highScore = 1;
int defaultLevel = 0;
int level = defaultLevel;
int difficulty = level;
int numCollectables = 10;
int collectablesCreated =  0;
int collectablesOnScreen;

//Level creation
int tileSize = 32 * 1;
int levelRows = 31;
int levelColumns = 22;
int levelPadX = tileSize/2;
int levelPadY = tileSize * 2;

//Death anim speed
int deathAnimSpeed = 10;
int deathAnimTick = 0;
enum direction {dirdown, dirleft,dirup,dirright};

direction robotDirection = dirdown;

//Game State
enum state {vindince,det,ld,intro,title,levelselect,controls,startgame,game,quit,done};
state GAMESTATE;


//lighting variables
int defaultLight = ((tileSize) + 10 ) * 2;
int scoutLight = ((tileSize) + 10 )  * 6;
int shiftLight = ((tileSize) + 10 )  * 1;

int deathLight = 0;
int deathLightSpeed = 1;
int winLight = 100;

int enemyLightSpread = 0;

int lightSpeed = tileSize * .5;
int lightingSpread = 100;

int transitionTimer = 3000;
int transitionTime = 3000;

int fragmentation = 100;
bool scoutMode = false;
bool exitingScoutMode = false;
float scoutResetSpeed = 1.5;
bool shiftMode = false;
bool shiftModeError = false;
bool finishedVert = false ;
bool finishedHor = false ;

float defaultSpeed = .3;
float scoutSpeed = .5;
float shiftSpeed =  .4;

//The event structure that will be used
SDL_Event event;

//Layers
vector<Sprite*> collectables;
vector<Sprite*> enemies;
vector<Sprite*> trail;

//Create Map data
vector<Sprite*> map;

//Create collision map
vector<Sprite*> collisionMap;

//Create sprites
vector<Sprite*> sprites;

Sprite* spritePlayer = NULL;
Sprite* spriteScout = NULL;
Sprite* spriteShift = NULL;

//Input Section
SDL_Joystick *stick = NULL;
float speed = 1;
bool kleft,kright,kup,kdown;
bool jleft,jright,jup,jdown;
bool jbutton1,jbutton2,jbutton3,jbutton4;
bool isMoving = false;
bool canAction = false;
void PrintKeyInfo ( SDL_KeyboardEvent *key);
void debug();
void printJoystickInfo();
void printJoystickState();
void detectJoystick();

void init();
bool loadFiles();

void handleTiming();
void handleMovement();
void handleKeyboard();
void handleJoystick();
void combineInput();
void handleUpdates();
void handleInteractions();
void handleEnemies();
void setLighting();

void showCharge();

void setStartingPosition();

void handleRender();


bool checkCollision(SDL_Rect a, SDL_Rect b);
void exitScoutMode();

void renderTestText();

void resetLevel();
void createLevel();
void placeCollectables();
void placeTraps();
void placeEnemies();
void placeEnemy(int x, int y, int type, float speed);
void muteAudio();

//Loads image from filename, returns final optimized image
SDL_Surface *loadImage( std::string filename){
    //Main loaded image
    SDL_Surface* loadedImage = NULL;
    //Optimized image for return
    SDL_Surface* optimizedImage = NULL;
    
    loadedImage = IMG_Load(filename.c_str());
    
    if (loadedImage != NULL) {
        optimizedImage = SDL_DisplayFormatAlpha(loadedImage);
        SDL_FreeSurface(loadedImage);
    }
    
    return optimizedImage;
}


//Applies one surface to another by position
void applySurface(int x ,int y , SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip = NULL){
    SDL_Rect offset;
    
    offset.x = x;
    offset.y = y;

    SDL_BlitSurface(source , clip , destination , &offset);
}



//Clean up resources when quitting
void cleanUp(){
    //Free surfaces
    SDL_FreeSurface(sprite);
    SDL_FreeSurface(robotUp);
    SDL_FreeSurface(robotScout);

    SDL_FreeSurface(screen);
    
    SDL_JoystickClose(stick);
    
    Mix_FreeMusic(levelIntroMusic);
    Mix_FreeChunk(sound1);
    Mix_Quit();
    
    //Quit when finished
    SDL_Quit();
}

int SDL_main (int argc, char **argv) {
    enum titleOptions {begin, control,exit};
    titleOptions optionSelected = begin;
    
    enum difficulties {low,medium,high};
    
    difficulties difficultySelected = low;
    
    
    init();


    
    //Song begin position
    //Mix_SetMusicPosition(20);
    
    //Play opening music
    Mix_PlayMusic(titleMusic, -1);
    Mix_PlayMusic(titleMusic, -1);
    
    GAMESTATE = vindince;
    //GAME LOOP ENTRY//
    while(GAMESTATE != done)
    {

        if (DEBUGMODE){
            debug();
        }
        
        
        //Handle Game Logic
        switch (GAMESTATE) {
            case game:
                
                if (!Mix_PlayingMusic()){
                    introPlayed = true;
                }
                
                if (introPlayed && !Mix_PlayingMusic()){
                    Mix_PlayMusic(levelMusic,-1);
                    
                }
                
                handleTiming();
                handleJoystick();
                handleKeyboard();
                combineInput();
                if (!death){
                    handleMovement();
                }
                handleEnemies();
                handleUpdates();
                showCharge();
                handleInteractions();
                setLighting();
                handleRender();
                break;
                
            case startgame:
                Mix_PlayMusic(levelIntroMusic,0);
                introPlayed = false;
                death = false;
                GAMESTATE=game;
                totalSeconds = 11;
                resetLevel();
                break;
                
                
            case vindince:
                handleTiming();
                transitionTimer-=animDelta;
                handleJoystick();
                handleKeyboard();
                combineInput();
                applySurface(0, 0, vinScreen, screen);
                
                if ((button2.isPressed && !button2.isHeld) || transitionTimer <=0){
                    GAMESTATE = det;
                    transitionTimer = transitionTime;
                }
                break;
                
                
            case det:
                handleTiming();
                transitionTimer-=animDelta;
                handleJoystick();
                handleKeyboard();
                combineInput();
                applySurface(0, 0, detScreen, screen);
                
                if ((button2.isPressed && !button2.isHeld) || transitionTimer <=0) {
                    GAMESTATE = ld;
                    transitionTimer = transitionTime;
                }
                break;
                
            
            case ld:
                handleTiming();
                transitionTimer-=animDelta;
                handleJoystick();
                handleKeyboard();
                combineInput();
                applySurface(0, 0, ldScreen, screen);
                
                if ((button2.isPressed && !button2.isHeld) || transitionTimer <=0) {
                    Mix_PlayChannel(-1, startupSound, 0);
                    GAMESTATE = intro;
                    transitionTimer = transitionTime*4;
                }
                break;

                
            case intro:
                handleTiming();
                transitionTimer-=animDelta;
                handleJoystick();
                handleKeyboard();
                combineInput();
                applySurface(0, 0, introScreen, screen);
                
                if ((button2.isPressed && !button2.isHeld) || transitionTimer <=0) {
                    GAMESTATE = title;
                    transitionTimer = transitionTime;
                }

                break;
            
            case title:
                Mix_HaltChannel(4);

                handleTiming();
                transitionTimer-=animDelta;
                handleJoystick();
                handleKeyboard();
                combineInput();
                applySurface(0, 0, titleScreen, screen);
                
                if ((button1.isPressed && !button1.isHeld) || (upButton.isPressed && !upButton.isHeld)){
                    optionSelected--;
                    Mix_PlayChannel(-1, countDownSound, 0);
                    if (optionSelected < 0){
                        optionSelected = exit;
                    }
                }
                
                if ((button1.isPressed && !button1.isHeld) || (downButton.isPressed && !downButton.isHeld)){
                    optionSelected++;
                    Mix_PlayChannel(-1, countDownSound, 0);
                    if (optionSelected >= 3){
                        optionSelected = begin;
                    }
                }
                
                
                
                switch (optionSelected) {
                    case begin:
                        applySurface(350, 400, bitLarge, screen);
                        if (button2.isPressed && !button2.isHeld){
                            GAMESTATE = levelselect;
                            Mix_PlayChannel(-1, countDownSound, 0);
                        }
                        
                        break;
                        
                    case control:
                        applySurface(350, 480, bitLarge, screen);
                        if (button2.isPressed && !button2.isHeld){
                            GAMESTATE = controls;
                            Mix_PlayChannel(-1, countDownSound, 0);
                        }
                        break;
                        
                    case exit:
                        applySurface(350, 560, bitLarge, screen);
                        if (button2.isPressed && !button2.isHeld){
                            transitionTimer = transitionTime ;
                            GAMESTATE = quit;
                            death = false;
                            Mix_HaltMusic();
                            Mix_HaltChannel(-1);
                            Mix_PlayChannel(-1, quitSound, 0);
                        }
                        break;
                        
                    default:
                        break;
                }
                

                break;
                
            case levelselect:
                handleTiming();
                transitionTimer-=animDelta;
                handleJoystick();
                handleKeyboard();
                combineInput();
                applySurface(0, 0, levelSelectScreen, screen);
                
                if ((button1.isPressed && !button1.isHeld) || (downButton.isPressed && !downButton.isHeld)){
                    difficultySelected++;
                    Mix_PlayChannel(-1, countDownSound, 0);
                    if (difficultySelected >= 3){
                        difficultySelected = low;
                    }
                }
                
                if ((button1.isPressed && !button1.isHeld) || (upButton.isPressed && !upButton.isHeld)){
                    difficultySelected--;
                    Mix_PlayChannel(-1, countDownSound, 0);
                    if (difficultySelected < 0){
                        difficultySelected = high;
                    }
                }
                
                
                switch (difficultySelected) {
                    case low:
                        applySurface(350, 260, bitLarge, screen);
                        if (button2.isPressed && !button2.isHeld){
                            GAMESTATE = startgame;
                            defaultLevel = 0;
                            level = defaultLevel;
                            Mix_PlayChannel(-1, countDownSound, 0);
                        }
                        
                        break;
                        
                    case medium:
                        applySurface(350, 420, bitLarge, screen);
                        if (button2.isPressed && !button2.isHeld){
                            GAMESTATE = startgame;
                            defaultLevel = 10;
                            level = defaultLevel;
                            Mix_PlayChannel(-1, countDownSound, 0);
                        }
                        break;
                        
                    case high:
                        applySurface(350, 580, bitLarge, screen);
                        if (button2.isPressed && !button2.isHeld){
                            transitionTimer = transitionTime * 2;
                            GAMESTATE = startgame;
                            defaultLevel = 20;
                            level = defaultLevel;
                            Mix_PlayChannel(-1, countDownSound, 0);
                        }
                        break;
                        
                        default:
                        break;
                }
                
                break;
                
            case controls:
                handleTiming();
                transitionTimer-=animDelta;
                handleJoystick();
                handleKeyboard();
                combineInput();
                applySurface(0, 0, controlsScreen, screen);
                detectJoystick();
                if (joystickActive){
                    applySurface(480, 580, xboxActive, screen);
                } else {
                    applySurface(480, 580, xboxInactive, screen);
                }
                
                
                if ((button2.isPressed && !button2.isHeld)) {
                    GAMESTATE = title;
                    transitionTimer = transitionTime;
                }
                break;
        

            case quit:
                death = false;
                handleTiming();
                transitionTimer-=animDelta;
                handleJoystick();
                handleKeyboard();
                combineInput();
                applySurface(0, 0, quitScreen, screen);
                
                if ((button2.isPressed && !button2.isHeld) || transitionTimer <=0) {
                    GAMESTATE = done;
                    transitionTimer = transitionTime;
                }
                break;
                
            default:
                break;
        }


        
        
        
        
        //Update Screen
        SDL_Flip(screen);
        totalFrames++;

        
    } //End game loop
    
    //Clean up then quit app
    cleanUp();
    SDL_Quit();
    return 0;
}



void detectJoystick(){
    //Check for then load joystick
    SDL_JoystickUpdate();
    
    if (SDL_NumJoysticks() > 0) {
        stick = SDL_JoystickOpen(0);
    }
    
    if (stick == NULL) {
        printf("Problem connecting to joystick/gamepad \n");
        joystickActive = false;
    } else {
        printJoystickInfo();
        joystickActive = true;
    }
}



void handleJoystick(){
    //HANDLE JOYSTICK INPUT
    jup = jdown = jleft = jright  = false;
    
    if ( SDL_JoystickGetButton( stick, 0 ) )  {
        jup = true;
        upButton.press();
    }
    
    
    if ( SDL_JoystickGetButton( stick, 1 ) )  {
        jdown = true;
        downButton.press();
    }
    
    
    if ( SDL_JoystickGetButton( stick, 2 ) )  {
        jleft = true;
    }
    
    if ( SDL_JoystickGetButton( stick, 3 ) )  {
        jright = true;
    }
    
    if ( SDL_JoystickGetButton( stick, 4 ) )  {
        jup = true;
    }
    
    
    if ( SDL_JoystickGetButton( stick, 11 ) )  {
        //Pressed A button
        joyButton2.press();
    } else {
        joyButton2.release();
    }
    
    
    if ( SDL_JoystickGetButton( stick, 12 ) )  {
        //Pressed B button
        joyButton1.press();
    } else {
        joyButton1.release();
    }
    
}




void handleKeyboard(){
    //New Key state code
    //Capture all keystates
    Uint8 *keystates = SDL_GetKeyState(NULL);
    
    //Did player press Z?
    if (keystates[SDLK_z]){
        button2.press();
    }else {
        button2.release();
    }
    

    if (keystates[SDLK_UP]){
        upButton.press();
    }else {
        upButton.release();
    }

    if (keystates[SDLK_DOWN]){
        downButton.press();
    }else {
        downButton.release();
    }
    
    
    //Did player press X?
    if (keystates[SDLK_x]){
        button1.press();
    }else {
        button1.release();
    }
    
    
    //Did player press X?
    if (keystates[SDLK_RETURN]){
        enterButton.press();
    }else {
        enterButton.release();
    }
    
    
    //Did player press f?
    if (keystates[SDLK_f]){
        fullScreenButton.press();
    }else {
        fullScreenButton.release();
    }
    
    //Did player press f?
    if (keystates[SDLK_ESCAPE]){
        escapeButton.press();
    }else {
        escapeButton.release();
    }
    
    
    // Respond to any events that occur
    while(SDL_PollEvent(&event))
    {
        string keyName;
        
        switch(event.type)
        {
                
            case SDL_MOUSEBUTTONDOWN:
                
                if (event.button.button == SDL_BUTTON_LEFT){
                    mouseButton2.isHeld = true;
                }
                
                if (event.button.button == SDL_BUTTON_RIGHT){
                    mouseButton1.isHeld = true;

                }
                
                break;
                
                
            case SDL_MOUSEBUTTONUP:
                
                if (event.button.button == SDL_BUTTON_LEFT){
                    mouseButton2.release();
                }
                
                if (event.button.button == SDL_BUTTON_RIGHT){
                    mouseButton1.release();
                }
                
                break;
                
                
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym){
                        
                    case SDLK_LEFT:
                        kleft = true;
                        break;
                        
                    case SDLK_RIGHT:
                        kright = true;
                        break;
                        
                    case SDLK_UP:
                        kup = true;
                        break;
                        
                    case SDLK_DOWN:
                        kdown = true;
                        break;
                        
                        
                    case SDLK_a:
                        kleft = true;
                        break;
                        
                    case SDLK_d:
                        kright = true;
                        break;
                        
                    case SDLK_w:
                        kup = true;
                        break;
                        
                    case SDLK_s:
                        kdown = true;
                        break;

                        
                    default:
                        break;
                }
                
                break;
                
                
            case SDL_KEYUP:
                switch (event.key.keysym.sym) {
                        
                    case SDLK_RIGHT:
                        kright = false;
                        break;
                        
                    case SDLK_LEFT:
                        kleft = false;
                        break;
                        
                    case SDLK_UP:
                        kup = false;
                        break;
                        
                    case SDLK_DOWN:
                        kdown = false;
                        break;
                        
                        
                        
                    case SDLK_a:
                        kleft = false;
                        break;
                        
                    case SDLK_d:
                        kright = false;
                        break;
                        
                    case SDLK_w:
                        kup = false;
                        break;
                        
                    case SDLK_s:
                        kdown = false;
                        break;
                        
                    default:
                        break;
                }
                
                break;
                
            case SDL_QUIT:
                Mix_HaltChannel(-1);
                Mix_HaltMusic();
                Mix_PlayChannel(-1, quitSound, 0);
                GAMESTATE = quit;
                transitionTimer = transitionTime;
                break;
                
                
            default:
                break;
        }
    }
    
    // Check for escape
//    Uint8 *keys = SDL_GetKeyState(NULL);
    
}


void showCharge(){
    
    for (int i = 0; i < 10 ; i++){
        cEmpty[i]->position.x = (spritePlayer->position.x) - 40 ;
        cEmpty[i]->position.y = spritePlayer->position.y - (i * 8 ) + 40;
    }
    
    
    for (int i = 0; i < 10 ; i++){
        c[i]->isDrawn = false;
        c[i]->position.x = (spritePlayer->position.x) - 40 ;
        c[i]->position.y = spritePlayer->position.y - (i * 8 ) + 40;
        
        if (currentSeconds > 6){
            for (int u = 0; u < 10 ; u++){
                c[i]->image = charge;
            }
        }
        
        if (currentSeconds <= 6 && currentSeconds >=4){
            for (int u = 0; u < 10 ; u++){
                c[i]->image = charge2;
            }
        }
        
        
        if (currentSeconds < 4 ){
            for (int u = 0; u < 10 ; u++){
                c[i]->image = charge3;
            }
        }
        
        if (currentSeconds <=1){
            for (int u = 0; u < 10 ; u++){
                c[i]->image = charge4;
            }
        }
        
        
    }
    
    for (int i = 0 ; i < currentSeconds ; i++){
        c[i]->isDrawn = true;
    }
    
    
    
}

void combineInput(){
    //Combine Joy and buttons and mouse
    if (joyButton1.isHeld || mouseButton1.isHeld){button1.isHeld = true;}
    
    if (joyButton2.isHeld
        || mouseButton2.isHeld
        || enterButton.isHeld
        ){button2.isHeld = true;}
    
    
    
    
    if (joyButton1.isPressed || mouseButton1.isPressed ){button1.isPressed = true;}
    if (joyButton2.isPressed
        || mouseButton2.isPressed
        || enterButton.isPressed
        ){button2.isPressed = true;}
    
    
    //handle fullscreen switch
    if (fullScreenButton.isPressed && !fullScreenButton.isHeld){
        if(FULLSCREEN){
            FULLSCREEN = false;
            SDL_ShowCursor(1);
            screen = SDL_SetVideoMode(WIDTH, HEIGHT, BPP, SDL_HWACCEL );
        }
        
        if (!FULLSCREEN){
            SDL_ShowCursor(0);
            screen = SDL_SetVideoMode(WIDTH, HEIGHT, BPP, SDL_HWACCEL | SDL_FULLSCREEN);
            FULLSCREEN = true;
        }
        
    }
    
    
    if(escapeButton.isPressed && !escapeButton.isHeld ) {
        printf("escape...\n" );
        
        
        if (GAMESTATE == title)
        {
            GAMESTATE = quit;
            Mix_HaltMusic();
        }
        
        if (GAMESTATE !=title && GAMESTATE !=quit)
        {
            GAMESTATE = title;
            Mix_HaltMusic();
            Mix_PlayMusic(titleMusic,-1);
        }
        
    }
    
    
    
}

void handleMovement(){
    
    
    
    if (shiftMode && !button2.isHeld && !shiftModeError){
        spritePlayer->position = spriteShift->position;
        Mix_PlayChannel(-1, resetShiftSound, 0);
    }
    
    if (shiftMode && !button2.isHeld && shiftModeError){
        Mix_PlayChannel(-1, shiftErrorSound, 0);
        shiftModeError = false;
    }
    
    
    if (scoutMode && !button1.isHeld){
        Mix_PlayChannel(-1, resetScoutSound, 0);
    }
    
    shiftMode = button2.isHeld;
    
    if (!button1.isHeld && scoutMode){
        exitingScoutMode = true;
        finishedVert = false ;
        finishedHor = false ;
    }
    
    //If scout mode is off you can turn scoutmode back on
    if (!exitingScoutMode){
        scoutMode = button1.isHeld;
    }

    //draw the scout if exiting mode is off
    if (!exitingScoutMode){
        spriteScout->isDrawn = scoutMode;
    }
    
    if (shiftMode){
        shiftModeError = false;
        for (vector<Sprite*>::iterator i = collisionMap.begin() ; i < collisionMap.end() ; i++ ){
            if (checkCollision(spriteShift->position, (*i)->position)
                || spriteShift->position.x > WIDTH - tileSize
                || spriteShift->position.x <= tileSize
                || spriteShift->position.y > HEIGHT - tileSize
                || spriteShift->position.y < 0 + tileSize * 2
                )  {
                shiftModeError = true;
                break;
            }
        }
        
    }
    
    
    spriteShift->isDrawn = shiftMode;
    
    if (button1.isPressed && !button1.isHeld){
        if (!Mix_Playing(5)){
            Mix_PlayChannel(5, enterScoutSound, 0);
        }
    }
    
    if (button2.isPressed && !button2.isHeld){
        if (!Mix_Playing(5)){
            Mix_PlayChannel(5, enterShiftSound, 0);
        }
    }
    
    spritePlayer->isAccelerating = false;
    
    if ((scoutMode || shiftMode) && (!exitingScoutMode)){
        
        //Change speeds for different modes
        spritePlayer->velX = 0;
        spritePlayer->velY = 0;
        
        spritePlayer->maxVelX =  scoutMode ? scoutSpeed : spritePlayer->maxVelX;
        spritePlayer->maxVelY =  scoutMode ? scoutSpeed : spritePlayer->maxVelY;
        
        spritePlayer->maxVelX =  shiftMode ? shiftSpeed : spritePlayer->maxVelX;
        spritePlayer->maxVelY =  shiftMode ? shiftSpeed : spritePlayer->maxVelY;
        
    } else {
        spritePlayer->maxVelX =  defaultSpeed;
        spritePlayer->maxVelY =  defaultSpeed;

    }
    
    //if special mode turned on then make player scroll with background
    if (scoutMode || shiftMode || exitingScoutMode){
        spritePlayer->doesScroll = true;
    } else {
        spritePlayer->doesScroll = false;
    }
    
    spriteScout->maxVelX = spritePlayer->maxVelX;
    spriteScout->maxVelY = spritePlayer->maxVelY;
    
    spriteShift->maxVelX = spritePlayer->maxVelX;
    spriteShift->maxVelY = spritePlayer->maxVelY;
    
    
    if (kright || jright){
        spritePlayer->image = robotRight;
        spriteScout->image = scoutRight;
        spriteShift->image = shiftRight;

        if (shiftModeError){
             spriteShift->image = errorRight;
        }
        
        
        spriteScout->velX += spritePlayer->accelX;
        spriteShift->velX += spritePlayer->accelX;
        
        if (!scoutMode && !shiftMode){
        spritePlayer->velX += spritePlayer->accelX;
        spritePlayer->isMoving = true;
        spritePlayer->isAccelerating = true;
        }
    }
    
    if (kleft || jleft){
        spritePlayer->image = robotLeft;
        
        spriteScout->image = scoutLeft;
        spriteShift->image = shiftLeft;
        
        if (shiftModeError){
            spriteShift->image = errorLeft;
        }
        
        spriteScout->velX -= spritePlayer->accelX;
        spriteShift->velX -= spritePlayer->accelX;
        
        if (!scoutMode && !shiftMode){
        spritePlayer->velX -= spritePlayer->accelX;
        spritePlayer->isMoving = true;
        spritePlayer->isAccelerating = true;
        }
    }
    
    if (kup || jup){
        
        spritePlayer->image = robotUp;
        
        spriteScout->image = scoutUp;
        spriteShift->image = shiftUp;
        
        if (shiftModeError){
            spriteShift->image = errorUp;
        }
        
        spriteScout->velY -= spritePlayer->accelY;
        spriteShift->velY -= spritePlayer->accelY;
        
        if (!scoutMode && !shiftMode){
        spritePlayer->velY -= spritePlayer->accelY;
        spritePlayer->isMoving = true;
        spritePlayer->isAccelerating = true;
        }
    }
    
    if (kdown || jdown){
        spritePlayer->image = robotDown;
        spriteScout->image = scoutDown;
        spriteShift->image = shiftDown;
        
        if (shiftModeError){
            spriteShift->image = errorDown;
        }
        
        spriteScout->velY += spritePlayer->accelY;
        spriteShift->velY += spritePlayer->accelY;
        
        if (!scoutMode && !shiftMode){
        spritePlayer->velY += spritePlayer->accelY;
        spritePlayer->isMoving = true;
        spritePlayer->isAccelerating = true;
        }
    }
}

void resetLevel(){
    coolDownTimer = coolDownTime;
    
   // printf("Level: %i\n" , level);
    lightingSpread = 600;
    collectablesCreated = 0;
    collectablesOnScreen = 0;
    numCollectables = 0;
    
    map.clear();
    collectables.clear();
    collisionMap.clear();
    enemies.clear();
    trail.clear();
    
    createLevel();
    placeCollectables();
    placeTraps();
    placeEnemies();
    
    setStartingPosition();
    spriteShift->position = spritePlayer->position;
    spriteScout->position = spritePlayer->position;
    currentSeconds = 11;
    totalSeconds = 11;
    
    if(!Mix_Playing(4)){
        Mix_PlayChannel(4, startLevel, 0);
    }
}


void handleUpdates(){
    
    coolDownTimer-= animDelta;
    
    if (coolDownTimer < 0){
        coolDownTimer = 0;
    }
    
    if (coolDownTimer > 0){
        spritePlayer->isDrawn =!spritePlayer->isDrawn;
    } else {
        spritePlayer->isDrawn = true;
    }
    
    if (head->position.x > 2000){
        head->isDrawn = false;
        head->velX = 0;
        head->maxVelX= 0;
    }
    

    currentSeconds = totalSeconds;

    tear->velY = .1;
    
    tearTimer-=animDelta;
    if (tearTimer <0){
        tearTimer = 0;
    }
    
    if (tearTimer > 0 && totalSeconds <5){
        tear->isDrawn= true;

    } else {
        tear->isDrawn = false;
        
    }

    //draw trail
    if (0){
        trailDistance = 0;
        Sprite* newTrail = new Sprite;
        
        newTrail->position.x = spritePlayer->position.x;
        newTrail->position.y = spritePlayer->position.y + spritePlayer->position.h ;
        newTrail->image = trailImage;
        
        trail.push_back(newTrail);
    }
    
    if (collectablesOnScreen == 1 && !death){
        if (!Mix_Playing(4)){
            Mix_PlayChannel(4, fragFound, 0);
        }
        collectables[0]->image = finalDataImage;
    }
    
    if (collectablesOnScreen <=0){
        level++;
        
        if (level > highScore){
            Mix_PlayChannel(4, highScoreSound, 0 );
            highScore = level;
        }
        
        resetLevel();
    }
    
    
    fragmentation =  ( ((float)collectablesOnScreen /  (float)numCollectables) * 100 );
    
    if (exitingScoutMode){
        exitScoutMode();
    }
    

    
    //Iterate over sprites to update
    for ( vector<Sprite*>::iterator i = sprites.begin() ; i != sprites.end() ; i ++){
        (*i)->Update(animDelta);
    }
    
    //Iterate over sprites to update
    for ( vector<Sprite*>::iterator i = enemies.begin() ; i != enemies.end() ; i ++){
        (*i)->Update(animDelta);
    }
    

    if (death){
        
        spriteScout->isDrawn = false;
        spriteShift->isDrawn = false;
        
        spritePlayer->velX = 0;
        spritePlayer->velY = 0;
        
        spriteBubble->position.x = spritePlayer->position.x + 10;
        spriteBubble->position.y = spritePlayer->position.y - 25;
        spriteBubble->isDrawn = true;
        
        deathAnimTick++;
        
        if (deathAnimTick>= deathAnimSpeed){
            deathAnimTick = 0;
            robotDirection++;
            
            if (robotDirection > dirright){
                robotDirection = dirdown;
            }
        }
        
        
        switch (robotDirection) {
            case dirup:
                spritePlayer->image = robotUp;
                break;
                
            case dirdown:
                spritePlayer->image = robotDown;
                break;
                
            case dirleft:
                spritePlayer->image = robotLeft;
                break;
                
            case dirright:
                spritePlayer->image = robotRight;
                break;
                
            default:
                break;
        }
        
        
        if (lightingSpread > deathLight){
            lightingSpread-=deathLightSpeed;
        }
    } else {
        spriteBubble->isDrawn = false;
    }
    
    if (death && lightingSpread <=0 ){
        level = defaultLevel;
        resetLevel();
        death = false;
    }
    
    //Position shadows when not in their modes
    if (scoutMode && !death){
        if (lightingSpread < scoutLight){
            lightingSpread +=lightSpeed;
        }

        if (lightingSpread >= scoutLight){
            lightingSpread = scoutLight;
        }
        
        spriteShift->position = spritePlayer->position;
    } else {
        spriteScout->position = spritePlayer->position;
    }
    
    
    
    if (shiftMode && !death){
        
        if (lightingSpread > shiftLight){
            lightingSpread-=lightSpeed;
        }
        
        if (lightingSpread <= shiftLight){
            lightingSpread = shiftLight;
        }
        
        
        spriteScout->position = spritePlayer->position;
    } else{
        spriteShift->position = spritePlayer->position;
    }
    
    if (!scoutMode && !shiftMode && !exitingScoutMode && !death){
        if (lightingSpread > defaultLight){
            lightingSpread-=lightSpeed;
        }
        
        if (lightingSpread < defaultLight){
            lightingSpread+=lightSpeed;
        }
        
    }

    
    
//    //Camera
//    Sprite* cameraTarget = NULL;
//    
//    if (scoutMode || exitingScoutMode){
//        cameraTarget = spriteScout;
//    } else if (shiftMode){
//        cameraTarget = spriteShift;
//    } else{
//        cameraTarget = spritePlayer;
//    }
//    
//    
//    
//    if (cameraTarget->position.x >= (WIDTH/2) ){
//        cameraPosition.x += cameraTarget->position.x - ( WIDTH/2) ;
//        cameraTarget->position.x -= cameraTarget->position.x - ( WIDTH/2);
//    }
//    
//
//    if (cameraTarget->position.x <= (WIDTH/2) ){
//        cameraPosition.x += cameraTarget->position.x - ( WIDTH/2) ;
//        cameraTarget->position.x -= cameraTarget->position.x - ( WIDTH/2);
//    }
//    
//    if (cameraTarget->position.y <= (HEIGHT/2) ){
//        cameraPosition.y += cameraTarget->position.y - ( HEIGHT/2) ;
//        cameraTarget->position.y -= cameraTarget->position.y - ( HEIGHT/2);
//    }
//    
//    if (cameraTarget->position.y >= (HEIGHT/2) ){
//        cameraPosition.y += cameraTarget->position.y - ( HEIGHT/2) ;
//        cameraTarget->position.y -= cameraTarget->position.y - ( HEIGHT/2);
//    }
    
    
    //If player is moving play sound, otherwise play stopping sound
    if (spritePlayer->isAccelerating && !death){
        if (!Mix_Playing(3)){
            Mix_PlayChannel(3, movingSound, -1);
        }
    } else {
        Mix_HaltChannel(3);
        if (spritePlayer->isMoving && !Mix_Playing(4)){
            Mix_PlayChannel(4, stoppingSound, 0);
        }

    }
    
    
    
}


void muteAudio(){
    for (int i = 0 ; i < 20 ; i ++ ){
        Mix_HaltChannel(-1);
        Mix_HaltChannel(i);
    }
}

void setLighting(){
    
       Sprite* lightTarget = NULL;
    
        if (scoutMode || exitingScoutMode){
            lightTarget = spriteScout;
        } else if (shiftMode){
            lightTarget = spriteShift;
        } else{
            lightTarget = spritePlayer;
        }
    
    
    //Add lighting based on player
    
    //change lighting for tiles
    for (vector<Sprite*>::iterator i = map.begin() ; i < map.end() ; i++){
        if (abs((*i)->position.x) - abs(lightTarget->position.x) <= lightingSpread
            &&abs((*i)->position.y) - abs(lightTarget->position.y) <= lightingSpread
            &&abs(lightTarget->position.x) - abs((*i)->position.x) <=lightingSpread
            &&abs(lightTarget->position.y) - abs((*i)->position.y) <=lightingSpread
                  
            ){
            (*i)->isDrawn = true;
        }
            else {
                (*i)->isDrawn = false;
            }
        }
    
    //change lighting for enemies
    for (vector<Sprite*>::iterator i = enemies.begin() ; i < enemies.end() ; i++){
        if (abs((*i)->position.x) - abs(lightTarget->position.x) <= lightingSpread*1.2
            &&abs((*i)->position.y) - abs(lightTarget->position.y) <= lightingSpread*1.2
            &&abs(lightTarget->position.x) - abs((*i)->position.x) <=lightingSpread*1.2
            &&abs(lightTarget->position.y) - abs((*i)->position.y) <=lightingSpread*1.2
            
            ){
            (*i)->image = orbImageActive;
            
            if (!(*i)->isSeeking && coolDownTimer <=0){
                Mix_PlayChannel(-1, activateSound, 0);
            }
            (*i)->isSeeking = true;
        }
        else {
            (*i)->image = orbImageInactive;
            (*i)->isSeeking = false;
        }
    }
    
    
    //change lighting for collectables
    for (vector<Sprite*>::iterator i = collectables.begin() ; i < collectables.end() ; i++){
        if (abs((*i)->position.x) - abs(lightTarget->position.x) <= lightingSpread + 10
            &&abs((*i)->position.y) - abs(lightTarget->position.y) <= lightingSpread+ 10
            &&abs(lightTarget->position.x) - abs((*i)->position.x) <=lightingSpread+ 10
            &&abs(lightTarget->position.y) - abs((*i)->position.y) <=lightingSpread+ 10
            
            
            ){
            (*i)->isDrawn = true;
        }
        else {
            (*i)->isDrawn = false;
        }
    }
    
    
    if (!death){
    for (vector<Sprite*>::iterator it = enemies.begin() ; it != enemies.end() ;it ++ ){
        lightTarget = (*it);
    //add lighting for enemies
    //change lighting for tiles
    for (vector<Sprite*>::iterator i = map.begin() ; i < map.end() ; i++){
        if (abs((*i)->position.x) - abs(lightTarget->position.x) <= enemyLightSpread
            &&abs((*i)->position.y) - abs(lightTarget->position.y) <= enemyLightSpread
            &&abs(lightTarget->position.x) - abs((*i)->position.x) <=enemyLightSpread
            &&abs(lightTarget->position.y) - abs((*i)->position.y) <=enemyLightSpread
            
            ){
            (*i)->isDrawn = true;
        }
        else {
           // (*i)->isDrawn = false;
        }
    }
    
    
    //change lighting for collectables
    for (vector<Sprite*>::iterator i = collectables.begin() ; i < collectables.end() ; i++){
        if (abs((*i)->position.x) - abs(lightTarget->position.x) <= enemyLightSpread
            &&abs((*i)->position.y) - abs(lightTarget->position.y) <= enemyLightSpread
            &&abs(lightTarget->position.x) - abs((*i)->position.x) <=enemyLightSpread
            &&abs(lightTarget->position.y) - abs((*i)->position.y) <=enemyLightSpread
            
            
            ){
            (*i)->isDrawn = true;
        }
        else {
            //(*i)->isDrawn = false;
        }
    }
        
    }
    }
    

}




//Load file test
bool loadFiles(){
    //Load Bitmap into sprite
    sprite = loadImage("robot2.png");
    robotUp = loadImage("bitup.png");
    robotDown = loadImage("bitdown.png");
    robotLeft = loadImage("bitleft.png");
    robotRight = loadImage("bitright.png");
    
    errorUp = loadImage("errorup.png");
    errorDown = loadImage("errordown.png");
    errorLeft = loadImage("errorleft.png");
    errorRight = loadImage("errorright.png");
    
    scoutUp = loadImage("scoutup.png");
    scoutDown = loadImage("scoutdown.png");
    scoutLeft = loadImage("scoutleft.png");
    scoutRight = loadImage("scoutright.png");
    
    shiftUp = loadImage("shiftup.png");
    shiftDown = loadImage("shiftdown.png");
    shiftLeft = loadImage("shiftleft.png");
    shiftRight = loadImage("shiftright.png");
    
    trailImage = loadImage("trail.png");
    deathBubble = loadImage("deathbubble.png");
    tearImage = loadImage("tear.png");
    
    
    robotScout = loadImage("robot3.png");
    robotShift = loadImage("robotnew3.png");
    robotShiftError = loadImage("robotnew4.png");
    dataImage = loadImage("floppy1.png");
    finalDataImage = loadImage("floppy2.png");
    
    divider = loadImage("divider1.png");

    charge = loadImage("charge1.png");
    charge2 = loadImage("charge2.png");
    charge3 = loadImage("charge3.png");
    charge4 = loadImage("charge4.png");
    emptyCharge = loadImage("charge5.png");
    
    headImage = loadImage("head.png");
    
    //Load screens
    vinScreen = loadImage("vindince.png");
    detScreen = loadImage("det.png");
    ldScreen = loadImage("ld27.png");
    introScreen = loadImage("intro1.png");
    titleScreen = loadImage("title.png");
    levelSelectScreen = loadImage("diff.png");
    controlsScreen = loadImage("controls.png");
    quitScreen = loadImage("kernelpanic.png");
    
    bitLarge = loadImage("bitlarge.png");
    
    xboxActive = loadImage("xboxactive.png");
    xboxInactive = loadImage("xboxinactive.png");
    
    controllerIcon = new Sprite();
    controllerIcon->image = xboxInactive;
    
    
    for (int i = 0; i < 10; i++) {
        c[i] = new Sprite();
        c[i]->image = charge;
    }
    
    for (int i = 0; i < 10; i++) {
        cEmpty[i] = new Sprite();
        cEmpty[i]->image = emptyCharge;
    }
    
    
    
    tile0 = loadImage("tile2.png");
    tile1 = loadImage("tile1.png");
    trap  = loadImage("tile0.png");
    orbImageActive  = loadImage("orb2.png");
    orbImageInactive  = loadImage("orb3.png");
    
    
    //Load up sprites
    spritePlayer = new Sprite();
    spriteScout = new Sprite();
    spriteShift = new Sprite();
    
    spritePlayer->accelX = .09;
    spritePlayer->accelY = .09;
    
    spritePlayer->dragX = .023;
    spritePlayer->dragY = .023;
    
    spriteScout->accelX = .09;
    spriteScout->accelY = .09;
    
    spriteScout->dragX = .023;
    spriteScout->dragY = .023;
    
    spriteShift->accelX = .09;
    spriteShift->accelY = .09;
    
    spriteShift->dragX = .023;
    spriteShift->dragY = .023;

    
    
    spritePlayer->loadImage("bitdown.png");
    spriteScout->loadImage("robotnew2.png");
    spriteShift->loadImage("robotnew3.png");

    spritePlayer->doesCollide = true;
    spriteScout->doesCollide = true;
    
    spritePlayer->position.x = WIDTH/2;
    spritePlayer->position.y = HEIGHT/2;
    
    sprites.push_back(spritePlayer);
    sprites.push_back(spriteScout);
    sprites.push_back(spriteShift);

    spriteBubble = new Sprite();
    spriteBubble->image = deathBubble;
    spriteBubble->isDrawn = false;
    
    
    sprites.push_back(spriteBubble);
    
    head = new Sprite();
    
    head->image = headImage;
    head->isDrawn = false;
    
    sprites.push_back(head);
    
    
    tear = new Sprite();
    
    tear->isDrawn = false;
    tear->image = tearImage;
    sprites.push_back(tear);

    
    sound1 = Mix_LoadWAV("swoosh.wav");
    resetSound = Mix_LoadWAV("invalidnode.wav");
    heartbeat = Mix_LoadWAV("swoosh.wav");
    movingSound = Mix_LoadWAV("stopsound1.wav");
    stoppingSound = Mix_LoadWAV("crunch1.wav");
    resetScoutSound = Mix_LoadWAV("swoosh2.wav");
    resetShiftSound = Mix_LoadWAV("swoosh.wav");
    enterScoutSound = Mix_LoadWAV("swoosh.wav");
    enterShiftSound = Mix_LoadWAV("swoosh2.wav");
    shiftErrorSound = Mix_LoadWAV("error1.wav");
    collectableSound = Mix_LoadWAV("pickup1.wav");
    startLevel = Mix_LoadWAV("begindefrag.wav");
    countDownSound = Mix_LoadWAV("squeek3.wav");
    activateSound = Mix_LoadWAV("shift1.wav");
    endTimeSound = Mix_LoadWAV("sigabort.wav");
    fragFound = Mix_LoadWAV("fragfound.wav");
    highScoreSound= Mix_LoadWAV("threadfound.wav");
    quitSound= Mix_LoadWAV("cpuhalt.wav");
    startupSound= Mix_LoadWAV("startup.wav");

    
    
    levelIntroMusic = Mix_LoadMUS("levelintro1.mp3");
    levelMusic = Mix_LoadMUS("levelsong1.mp3");
    titleMusic = Mix_LoadMUS("title1.mp3");

    if (sound1 == NULL) {
        printf("Failed to load sound file... \n");
    }
    
    if (levelIntroMusic == NULL){
        printf("Failed to load music file: %s\n" , Mix_GetError());
    }
    
    //Load font data
    font = TTF_OpenFont(FONTPATH "Verdana.ttf", 24);
    
    if (font ==NULL){
        printf("Error loading font...\n");
    }
    return true;
}


void init(){
    //Entry message to app, only defines OS
    if (CURRENTOS == 0){
        printf("Could not detect OS \n");
        WINDOW_TITLE = "LD27 - 10 Seconds - Undefined OS";
    }
    
    if (CURRENTOS == macos){
        printf("Current OS: Mac OS X \n");
        WINDOW_TITLE = "LD27 - 10 Seconds - Mac OS X";
    }
    
    if (CURRENTOS == windows){
        printf("Current OS: Windows \n");
        WINDOW_TITLE = "LD27 - 10 Seconds- Windows";
    }
    
    if (CURRENTOS == linux){
        printf("Current OS: Linux \n");
        WINDOW_TITLE = "LD27 - 10 Seconds - Linux";
    }
    
    //Init and setup SDL
    if ( SDL_Init(SDL_INIT_EVERYTHING) < 0 ){
        fprintf(stderr, "Unable to init SDL %s\n" , SDL_GetError());
        exit(1);
    }
    
    //Enable Unicode for reading keyboard
    //  SDL_EnableUNICODE(1);
    
    // Enable double-buffering
    // SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    
    // Create a window
    if (FULLSCREEN){
        screen = SDL_SetVideoMode(WIDTH, HEIGHT, BPP, SDL_HWACCEL | SDL_FULLSCREEN);
    } else {
        screen = SDL_SetVideoMode(WIDTH, HEIGHT, BPP, SDL_HWACCEL);
    }
    
    
    if(!screen)
    {
        printf("Couldn't set %dx%d GL video mode: %s\n", WIDTH,
               HEIGHT, SDL_GetError());
        SDL_Quit();
        exit(2);
    }
    SDL_WM_SetCaption(WINDOW_TITLE.c_str(), NULL);
    
    
    //Attempt to validate font library
    if (TTF_Init() == -1){
        printf("Failed to init font... \n");
    }
    
    //Open audio system
    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 4096) == -1 ) {
        printf("Failed to open audio system... \n");
    }
    
    //Init SDL Systems
    Mix_Init(MIX_INIT_MP3);
    IMG_Init(IMG_INIT_PNG);
    
    Mix_VolumeMusic(100);

    Mix_Volume(-1, 80);
    Mix_Volume(4, 200);
    
    //Load Bitmap into sprite through test function
    loadFiles();
    
    //Test Create level
    //resetLevel();
    

    detectJoystick();
    
    //Camera init
    
//    cameraPosition.x = 0;
//    cameraPosition.y = 0;
//    cameraPosition.w = WIDTH;
//    cameraPosition.h = HEIGHT;
    
}



void handleTiming(){
    //Update time deltas
    deltaTime += SDL_GetTicks() - startTime;
    animDelta = (SDL_GetTicks() - startTime);
    
    if (totalSeconds >= 10){
        totalSeconds = 10;
    }
    
    //Check for seconds incremented
    if (deltaTime >=1000){
        if (!shiftMode && !scoutMode && !exitingScoutMode && !death && GAMESTATE==game){
            totalSeconds--;
            Mix_PlayChannel(-1, countDownSound, -0);
            
            tearTimer = tearTime;
            tear->position.x = spritePlayer->position.x + 6;
            tear->position.y = spritePlayer->position.y - 2;
            
        } else {
            //currentSeconds--;
        }
        
        deltaTime = 0;
        FPS = totalFrames;
        totalFrames = 0;
    }
    
    
    //When 10 seconds run out
    if (totalSeconds <= 0) {
        totalSeconds = 11;
        muteAudio();
        Mix_HaltChannel(4);
        Mix_PlayChannel(4, endTimeSound, 0);
        death = true;
        
        head->position.x = 10;
        head->position.y = 0;
        
        head->velX =.8;
        head->maxVelX = .8;
        
        head->isDrawn = true;
        
    }
    
    //Update start of frame time
    startTime = SDL_GetTicks();
    
    
    // Animation code
    timer--;
    if (timer <= 0 ){
        timer = fullTimer;
        
        if (isMoving){
            frame++;
        } else {
            frame = 2;
        }
        if (frame >= fullClip) {
            frame = 0;
        }
    }
}



void handleRender(){
    
    //SDL DRAW LOOP
    //Black out background
    SDL_FillRect(screen, &screen->clip_rect, SDL_MapRGB(screen->format, 0, 0, 0));
    
    //Update camera offset
    //Iterate over sprites to draw
    for ( vector<Sprite*>::iterator i = map.begin() ; i != map.end() ; i ++){
        (*i)->cameraOffset = cameraPosition;
    }
    
    spritePlayer->cameraOffset = cameraPosition;
    
    //Iterate over tiles to draw
    for ( vector<Sprite*>::iterator i = map.begin() ; i != map.end() ; i ++){
        (*i)->draw(screen);
    }
    
    //iterate over trail to draw
    for ( vector<Sprite*>::iterator i = trail.begin() ; i != trail.end() ; i ++){
        (*i)->draw(screen);
    }
    
    
    //iterate over items to draw
    for ( vector<Sprite*>::iterator i = collectables.begin() ; i != collectables.end() ; i ++){
        (*i)->draw(screen);
    }
    
    
    //Iterate over sprites to draw
    for ( vector<Sprite*>::iterator i = sprites.begin() ; i != sprites.end() ; i ++){
        (*i)->draw(screen);
    }

    //Iterate over enemies to draw
    if (!death){
    for ( vector<Sprite*>::iterator i = enemies.begin() ; i != enemies.end() ; i ++){
        (*i)->draw(screen);
        }
    }
   
    //renderTestText();
    
    if (!death){
        for (int i = 0 ; i < 10 ; i++){
            cEmpty[i]->draw(screen);
        }
    }
    
    if (!death){
    for (int i = 0 ; i < 10 ; i++){
        c[i]->draw(screen);
        }
    }
    
    
}


void createLevel(){
    
    difficulty = level;
    //Create Tiles
    if (difficulty >= 50){
        difficulty = 50;
    }
    
    numCollectables = difficulty;
    
    if (numCollectables > 20){
        numCollectables = 20;
    }
    
    int tileTypes = 10 + (200 - difficulty) - (numCollectables * 2) ;
    
    int whichTile = 0;

    srand(int(time(NULL)));
    
    for (int i = 0 ; i < levelRows ; i++){
        for (int u = 0; u < levelColumns; u++) {
            Sprite* tile = new Sprite();
            
            whichTile = rand() % tileTypes;
            
            if (u == 0 || i == 0 || u == levelColumns -1 || i == levelRows -1  ){
                whichTile = 1;
            }
            
            if (whichTile <= 10) {
                tile->image = tile1;
                tile->isBoundary = true;
            } else {
                tile->image = tile0;
            }
            
            tile->position.x = levelPadX + (i * tileSize);
            tile->position.y = levelPadY + u * tileSize;
            tile->position.w = tileSize;
            tile->position.h = tileSize;
            tile->doesScroll = true;
            
            map.push_back(tile);
            
            if (tile->isBoundary){
                collisionMap.push_back(tile);
            }
            
        }
    }

    
    
    //Set player collision map
    spritePlayer->collisionMap = &collisionMap;
    //spriteScout->collisionMap = &collisionMap;

}

void placeCollectables(){
    //Add collectables
    Sprite *newCollectable = 0;
    int tileSelected = 0;
    
    //Populate with collectables
    
    while (collectablesCreated < numCollectables ){
        tileSelected = rand() % map.size();
        
        if (!map[tileSelected]->isBoundary){
            newCollectable = new Sprite();
            newCollectable->image = dataImage;
            newCollectable->isCollectable = true;
            
            newCollectable->position.x = map[tileSelected]->position.x + 8;
            newCollectable->position.y = map[tileSelected]->position.y + 8;
            
            collectablesCreated++;
            collectables.push_back(newCollectable);
            collectablesOnScreen++;
        }
        
    }
    
    }

void placeEnemies(){
    
    if (difficulty > 40) {
        difficulty = 40;
    }
    
    for (int i = 0 ; i < 1+(difficulty/2); i++ ){
        placeEnemy(rand() % WIDTH, rand() % HEIGHT , 1,.065 +  (difficulty)  * .00010);
    }
    
}

void placeEnemy(int x , int y , int type, float speed){
    Sprite* enemy = new Sprite();
    
    switch (type) {
        case 1:
            enemy->image = orbImageInactive;
            break;
            
        default:
            enemy->image = orbImageInactive;
            break;
    }
    
    enemy->position.x = x;
    enemy->position.y = y;

    enemy->maxVelX = speed;
    enemy->maxVelY = speed;

    enemy->velX = speed;
    enemy->velY = speed;
    
    enemy->isBad = true;
    
    enemies.push_back(enemy);
}


void handleEnemies(){
    for (vector<Sprite*>::iterator i = enemies.begin() ; i != enemies.end() ; i++ ){
      
    
        //Check for out of bounds
        if ((*i)->position.x >=WIDTH ||(*i)->position.x <= 0 ){
            (*i)->velX = -(*i)->velX;
        }
        
        if ((*i)->position.y >=HEIGHT ||(*i)->position.y <= 0 ){
            (*i)->velY = -(*i)->velY;
        }
    

        
        if ((*i)->isSeeking) {
            if ((*i)->position.x > spritePlayer->position.x){
                (*i)->velX = -(*i)->maxVelX;
            }
            
            if ((*i)->position.x < spritePlayer->position.x){
                (*i)->velX = (*i)->maxVelX;
            }
            
            if ((*i)->position.y > spritePlayer->position.y){
                (*i)->velY = -(*i)->maxVelY;
            }
            
            if ((*i)->position.y < spritePlayer->position.y){
                (*i)->velY = (*i)->maxVelY;
            }
            
            
            
        } else {
            
            int changeDirection = rand() % 100;
            if (changeDirection == 10 ){
                (*i)->velY = -(*i)->velY;
            }
            
            changeDirection = rand() % 100;
            if (changeDirection == 11 ){
                (*i)->velX = -(*i)->velX;
            }
        }
        
        
        
    }
}



void placeTraps(){
    //Add traps
    int tileSelected = 0;
    
    for (int i = 0 ; i < level/2 ; i++){
            tileSelected = rand() % map.size();
            for (vector<Sprite*>::iterator it = collectables.begin() ; it != collectables.end() ; it++){
                
                if (!checkCollision(map[tileSelected]->position,(*it)->position)){
                    map[tileSelected]->image = trap;
                    map[tileSelected]->isBad = true;
                    map[tileSelected]->isBoundary = false;
                    break;
                }
            }
        }
    
    for ( vector<Sprite*>::iterator i = map.begin() ; i != map.end() ; i++){
        if ((*i)->isBad){
            //look for overlap of collectable
            for (vector<Sprite*>::iterator it = collectables.begin() ; it != collectables.end() ; it++){
                if (checkCollision((*it)->position, (*i)->position)) {
                    (*i)->isBad = false;
                    (*i)->image = tile0;
                }
            }
        }
    }
    
    }
    



void handleInteractions(){
    if (!death){
    //check for interaction between player and collectables
    for ( vector<Sprite*>::iterator i = collectables.begin() ; i != collectables.end() ;){
        if ((*i)->isCollectable){
            //printf("check collectable...\n");
            if (checkCollision(spritePlayer->position, (*i)->position)){
                i = collectables.erase(i);
                collectablesOnScreen--;
                //printf("Found data file %i files left ! \n" , collectablesOnScreen);
                totalSeconds++;
                Mix_PlayChannel(-1, collectableSound, 0);
              
            } else{
                i++;
            }
        }
    }
    
    
    //check traps
    
     for ( vector<Sprite*>::iterator i = map.begin() ; i != map.end() ; i++){
         if ((*i)->isBad){
             SDL_Rect insideTrap = (*i)->position;
             insideTrap.h-=20;
             insideTrap.w-=16;
             
             insideTrap.x+=10;
             insideTrap.y+=5;

             
             //player hit trap
             if (checkCollision(spritePlayer->position, insideTrap) && coolDownTimer <= 0){
                 muteAudio();
                 Mix_HaltChannel(4);
                 Mix_PlayChannel(4, resetSound, 0);
                 death = true;
             }
         }
     }
    
        
        
    //check enemies
        for ( vector<Sprite*>::iterator i = enemies.begin() ; i != enemies.end() ; i++){
            if ((*i)->isBad && coolDownTimer <=0 ){
                //player hit enemy
                if (checkCollision(spritePlayer->position, (*i)->position)){
                    muteAudio();
                    Mix_PlayChannel(-1, resetSound, 0);
                    death = true;
                }
            }
        }
        
        
        
    }
    


}



bool checkCollision(SDL_Rect a, SDL_Rect b){
    
    //Compute left right top bottom of A and B
    
    int leftA , rightA , topA, bottomA ,leftB , rightB , topB, bottomB;
    
    leftA = a.x;
    rightA = a.x + a.w;
    topA = a.y;
    bottomA = a.y + a.h;
    
    leftB = b.x;
    rightB = b.x + b.w;
    topB = b.y;
    bottomB = b.y + b.h;
    
    //If any of the sides from A are outside of B
    if( bottomA <= topB )
    {
        return false;
    }
    
    if( topA >= bottomB )
    {
        return false;
    }
    
    if( rightA <= leftB )
    {
        return false;
    }
    
    if( leftA >= rightB )
    {
        return false;
    }
    
    //If none of the sides from A are outside B
   

    
   // printf("Did collide! \n");
    return true;
}


void exitScoutMode(){
    
    if (checkCollision(spritePlayer->position, spriteScout->position)){
        exitingScoutMode = false;
        scoutMode = false;
        //printf("Exiting scout mode \n");
    }
    
    if (spriteScout->position.x > spritePlayer->position.x){
        spriteScout->position.x-= scoutResetSpeed * animDelta;
    }
    
    if (spriteScout->position.x < spritePlayer->position.x){
        spriteScout->position.x+= scoutResetSpeed * animDelta;
    }
    
    if (spriteScout->position.y > spritePlayer->position.y){
        spriteScout->position.y-= scoutResetSpeed * animDelta;
    }
    
    if (spriteScout->position.y < spritePlayer->position.y){
        spriteScout->position.y+= scoutResetSpeed * animDelta;
    }
    
}


void setStartingPosition(){
    int selectedTile = rand() % (levelRows * levelColumns) ;
    
    while (map[selectedTile]->isBoundary || map[selectedTile]->isCollectable || map[selectedTile]->isCollectable){
        selectedTile = rand() % (levelRows * levelColumns) ;
    }
    
    startingPosition.x = map[selectedTile]->position.x + (spritePlayer->position.w/2) ;
    startingPosition.y = map[selectedTile]->position.y + (spritePlayer->position.h/2);
    
    spritePlayer->position = startingPosition;
}


//Debug functions-----------------------------------

void PrintKeyInfo( SDL_KeyboardEvent *key){
    //Is it a press or release?
    
    if (key->type == SDL_KEYUP){
       // printf("Released -  \n" );
    }else
        //printf("Pressed - \n");
    
    //Print Scan code
    //printf("Scancode : 0x%02X \n" , key->keysym.scancode );
    
    //Print name of the key
    //printf( ", Name: %s \n" , SDL_GetKeyName( key->keysym.sym ) );
    
    
    if (key->type == SDL_KEYDOWN ) {
        printf("Unicode: ");
        
        if (key->keysym.unicode < 0x80 && key->keysym.unicode>0 ) {
            printf ("%c (0x%04X) \n", (char)key->keysym.unicode , key->keysym.unicode );
        } else {
            printf("? (0x%04X) \n" , key->keysym.unicode);
        }
    }
    printf("\n");
}


void printJoystickInfo(){
    printf("Joystick info:\n");
    printf("Number of Joysticks: %i\n" ,SDL_NumJoysticks());
    printf("Joystick Name: %s \n" , SDL_JoystickName(0));
    printf("Axes: %i\n", SDL_JoystickNumAxes(stick));
    printf("Hats: %i\n", SDL_JoystickNumHats(stick));
    printf("Buttons: %i\n", SDL_JoystickNumButtons(stick));
}


void printJoystickState(){
    for ( int i = 0 ; i < SDL_JoystickNumButtons(stick) ; i++ ){
        printf( "Button %i: %i \n" , i ,SDL_JoystickGetButton( stick, i ) );
    }
}


void renderTestText(){
    sprintf(timeBuffer, "/var/structure/file0%d ", level);
    message = TTF_RenderText_Blended(font,timeBuffer, textColor);
    
    if (message == NULL){
      //  printf("Error rendering text... \n");
    }
    
    //Display message
    applySurface(400, 2, message, screen);

    
    if (highScore > 1){
    sprintf(timeBuffer, "/var/archive/file0%d ", highScore);
    message = TTF_RenderText_Blended(font,timeBuffer, textColor);
    
    if (message == NULL){
        //  printf("Error rendering text... \n");
    }
    
    //Display message
    applySurface(400, 28, message, screen);
}
    
    SDL_FreeSurface(message);
    
}



void debug(){
    //Print X Y coordinates
    printf("button 1 isPressed: %i , isHeld: %i , isReleased: %i \n " ,
           button1.isPressed , button1.isHeld , button1.isReleased);
    
    printf("button 2 isPressed: %i , isHeld: %i , isReleased: %i \n " ,
           button2.isPressed , button2.isHeld , button2.isReleased);
    
}








