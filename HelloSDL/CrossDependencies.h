//
//  CrossDependencies.h
//  SDL Skeleton
//
//  Created by Evan Chapman on 8/24/13.
//  Copyright (c) 2013 Student. All rights reserved.
//

#ifndef SDL_Skeleton_CrossDependencies_h
#define SDL_Skeleton_CrossDependencies_h


//OS Pre-processor definitions
//IF MAC OS
#if defined  __APPLE__ && __MACH__
#define CURRENTOS 1
#include <SDL_image/SDL_image.h>
#include <SDL_ttf/SDL_ttf.h>
#include <SDL_mixer/SDL_mixer.h>
#define FONTPATH "/Library/Fonts/"

//IF WINDOWS
#elif defined _WIN32
#define CURRENTOS 2
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#define FONTPATH "C:\Windows\Fonts\"

//IF LINUX
#elif defined __linux__
#define CURRENTOS 3

//IF UNDEFINED OS
#elif
#define CURRENTOS 0
#endif

enum OperatingSystems{macos = 1 ,windows = 2 ,linux = 3};

#include <stdlib.h>
#include <string>
#include <SDL/SDL.h>
#include <sstream>
#include <stdlib.h>
#include <time.h>

#endif
