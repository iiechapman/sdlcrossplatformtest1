//
//  button.h
//  SDL Skeleton
//
//  Created by Evan Chapman on 8/24/13.
//  Copyright (c) 2013 Student. All rights reserved.
//

#ifndef __SDL_Skeleton__button__
#define __SDL_Skeleton__button__

#include <iostream>

#endif /* defined(__SDL_Skeleton__button__) */


class Button{
    
public:
    void press();
    void release();
    
    bool isPressed;
    bool isHeld;
    bool isReleased;
};






















