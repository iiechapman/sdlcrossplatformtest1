//
//  Sprite.h
//  SDL Skeleton
//
//  Created by Evan Chapman on 8/24/13.
//  Copyright (c) 2013 Student. All rights reserved.
//

#ifndef __SDL_Skeleton__Sprite__
#define __SDL_Skeleton__Sprite__

#include <iostream>
#include <vector>
#include "CrossDependencies.h"
using namespace std;
#endif /* defined(__SDL_Skeleton__Sprite__) */


class Sprite{
public:
    Sprite();

    SDL_Rect position;
    
    SDL_Rect clip;
    SDL_Rect cameraOffset;
    
    SDL_Surface *image;
    
    vector<Sprite*> *collisionMap;
    
    float accelX,accelY;
    float maxVelX,maxVelY;
    float velX , velY;
    int finalX, finalY;
    float dragX, dragY;
    
    
    bool isSafe;
    
    bool isMoving;
    bool isAccelerating;
    bool hasStopped;
    
    bool isSeeking;
    
    bool doesScroll;
    bool isClipped;
    bool isBoundary;
    bool doesCollide;
    bool isDrawn;
    
    bool isBad;
    bool isCollectable;
    
    void Update(float deltaTime);
    bool loadImage(std::string filename);
    bool checkCollisions();
    bool detectCollision(SDL_Rect a , SDL_Rect b);
    void draw(SDL_Surface* screen);
};








